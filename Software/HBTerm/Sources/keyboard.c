/*
 * Copyright (C) 2020-2022 Jean-Michel Roux
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <termios.h>
#include <sys/time.h>
#include <unistd.h>

#include "keyboard.h"

static struct   termios initial_settings, new_settings;
static struct   timeval timeout;
static fd_set   read_handles;
static char     settings_ok;

int openKeyboard()
{
   if (tcgetattr(STDIN_FILENO, &initial_settings) < 0)
   {
      return -1;
   }
   else
   {
      settings_ok = 1;
   }

   new_settings = initial_settings;
   new_settings.c_lflag &= ~(ICANON | ECHO);
   new_settings.c_cc[VMIN] = 1;
   new_settings.c_cc[VTIME] = 0;

   if (tcsetattr(STDIN_FILENO, TCSANOW, &new_settings) < 0)
   {
      return -1;
   }

   timeout.tv_sec = timeout.tv_usec = 0;

   return 0;
}

void closeKeyboard()
{
   if (settings_ok)
   {
      tcsetattr(STDIN_FILENO, TCSANOW, &initial_settings);
   }
}

int kbhit()
{
   /* check stdin (fd 0) for activity */
   FD_ZERO(&read_handles);
   FD_SET(STDIN_FILENO, &read_handles);

   /* select uses fd + 1 */
   if (select(STDIN_FILENO + 1, &read_handles, NULL, NULL, &timeout) == -1)
   {
      return -1;
   }

   return (FD_ISSET(STDIN_FILENO, &read_handles)  ?  1  :  0);
}

int getch()
{
   unsigned char temp;

   /* stdin = fd 0 */
   if (read(STDIN_FILENO, &temp, 1) != 1)
   {
      return -1;
   }

   return (int)temp;
}
