/*
 * Copyright (C) 2020-2022 Jean-Michel Roux
 * Thanks to Vincent Rivière for his precious help!
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Version 2.2.2 is OK with macOS */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <termios.h>
#include <sys/time.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "keyboard.h"

#define VERSION         "2.2.2"
#define VERSION_LEN     5

#define BKSP            0x7f
#define ESC             0x1b
#define CR_LF_LEN       2
#define LOAD_LEN        4
#define CMD_LEN         4
#define PROMPT_LEN      15
#define HE_DATA         282
#define STR_UPLOAD_LEN  62
#define MSR_MAX_LEN     1024
#define UPLOAD          "--upload"
#define UPLOAD_LEN      8
#define DEVICE          "--device"
#define DEVICE_LEN      8
#define ARG_LEN         2
#define SPEED           "--speed"
#define SPEED_LEN       7
#define RETRY           "--retry"
#define RETRY_LEN       7
#define HELP            "--help"
#define HELP_LEN        6
#define UPLOAD_OK       0
#define FILE_NOT_FOUND  -1
#define INVALID_MSR     -2
#define BOARD_MUTE      -3
#define DATA_ERROR      -4
#define DATA_LEN        0x10
#define UART_BPS        57600
#define MAX_RETRY       10000

#if defined(__APPLE__) && defined(__MACH__)
/* macOS */
#define DEFAULT_USB     "/dev/tty.usbserial-AH01PAXI"
#elif defined (__gnu_linux__)
/* GNU/Linux */
#define DEFAULT_USB     "/dev/ttyUSB0"
#else
/* Cygwin */
#define DEFAULT_USB     "/dev/ttyS17"
#endif

struct timeval device_timeout;
fd_set device_read_handles;

char EchoBuffer[MSR_MAX_LEN];
char SRecordName[NAME_MAX + 1];
char Device[NAME_MAX + 1];
char Speed[SPEED_LEN];
int  nbRetry;

int  InvalidParameter(char *);
int  GetPrompt(int);
void delayMs(int);
int  UploadSRecord(const char *, int);

int main(int argc, char *argv[])
{
   struct termios termios_new, termios_org;
   int exit = 0;
   int speed = UART_BPS;

   int fd, tmp, l, n = 1;
   int RTS_flag = TIOCM_RTS;
   char c;

   strcpy(Device, DEFAULT_USB);
   nbRetry = MAX_RETRY;

   if (argc >= 2)
   {
      do
      {
         if (((argv[n][0] == '-' ) && ((argv[n][1] == 'u' ))) || !strncmp(argv[n], UPLOAD, UPLOAD_LEN))
         {
            if (argv[n][1] == 'u')
            {
               l = ARG_LEN;
            }
            else
            {
               l = UPLOAD_LEN;
            }

            if (strlen(&argv[n][l]) <= NAME_MAX)
            {
               if (argv[n][1] == 'u')
               {
                  strcpy(SRecordName, &argv[n][ARG_LEN]);
               }
               else
               {
                  strcpy(SRecordName, &argv[n][UPLOAD_LEN]);
               }
            }
            else
            {
               return(InvalidParameter(argv[n]));
            }
         }
         else if (((argv[n][0] == '-' ) && (argv[n][1] == 'd')) || !strncmp(argv[n], DEVICE, DEVICE_LEN))
         {
            if (argv[n][1] == 'd')
            {
               l = ARG_LEN;
            }
            else
            {
               l = DEVICE_LEN;
            }

            if (strlen(&argv[n][l]) <= NAME_MAX)
            {
               if (argv[n][1] == 'd')
               {
                  strcpy(Device, &argv[n][ARG_LEN]);
               }
               else
               {
                  strcpy(Device, &argv[n][DEVICE_LEN]);
               }
            }
            else
            {
               return(InvalidParameter(argv[n]));
            }
         }
         else if (((argv[n][0] == '-') && (argv[n][1] == 's')) || !strncmp(argv[n], SPEED, SPEED_LEN))
         {
            if (argv[n][1] == 's')
            {
               l = ARG_LEN;
            }
            else
            {
               l = SPEED_LEN;
            }

            if ((strlen(&argv[n][l]) >= (SPEED_LEN - 2)) && (strlen(&argv[n][l]) < SPEED_LEN))
            {
               strcpy(Speed, &argv[n][l]);

               if (!strcmp(Speed, "57600"))
               {
                  speed = 57600;
               }
               else if (!strcmp(Speed, "115200"))
               {
                  speed = 115200;
               }
               else
               {
                  return(InvalidParameter(argv[n]));
               }
            }
            else
            {
               return(InvalidParameter(argv[n]));
            }
         }
         else if (((argv[n][0] == '-') && (argv[n][1] == 'r')) || !strncmp(argv[n], RETRY, RETRY_LEN))
         {
            if (argv[n][1] == 'r')
            {
               l = ARG_LEN;
            }
            else
            {
               l = RETRY_LEN;
            }

            if ((strlen(&argv[n][l]) >= (RETRY_LEN - 2)) && (strlen(&argv[n][l]) < RETRY_LEN))
            {
               sscanf(&argv[n][l], "%d", &nbRetry);

               if (nbRetry < MAX_RETRY)
               {
                  return(InvalidParameter(argv[n]));
               }
            }
            else
            {
               return(InvalidParameter(argv[n]));
            }
         }
         else if (((argv[n][0] == '-') && (argv[n][1] == 'h') && (strlen(argv[n]) == ARG_LEN)) || !strncmp(argv[n], HELP, HELP_LEN))
         {
            tmp = write(STDOUT_FILENO, "HBTerm V", 8);
            tmp = write(STDOUT_FILENO, VERSION, VERSION_LEN);
            tmp = write(STDOUT_FILENO, " help:\r\n", 8);
            tmp = write(STDOUT_FILENO, "-d<arg> or --device<arg>    Set serial device name with <arg>.\r\n", 64);
            tmp = write(STDOUT_FILENO, "-h or --help                Display this information.\r\n", 55);
            tmp = write(STDOUT_FILENO, "-r<arg> or --retry<arg>     Set retry timeout with <arg>.\r\n", 59);
            tmp = write(STDOUT_FILENO, "-s<arg> or --speed<arg>     Set serial speed with <arg>.\r\n", 58);
            tmp = write(STDOUT_FILENO, "-u<arg> or --upload<arg>    Upload the file <arg>.\r\n", 52);
            return(0);
         }
         else
         {
            return(InvalidParameter(argv[n]));
         }
      }
      while (++n < argc);
   }

   /* Ouverture de la liaison série */
   if ((fd = open(Device, O_RDWR | O_NOCTTY | O_NONBLOCK)) == -1)
   {
      tmp = write(STDERR_FILENO, "Failed to open device ", 22);
      tmp = write(STDERR_FILENO, Device, strlen(Device));
      tmp = write(STDERR_FILENO, "\r\n", CR_LF_LEN);
      return(-1);
   }

   /* Lecture et sauvegarde des paramètres courants */
   tcgetattr(fd, &termios_new);
   tcgetattr(fd, &termios_org);
   /* On ignore les BREAK et les caractères avec erreurs de parité */
   termios_new.c_iflag = IGNBRK | IGNPAR;
   /* 8 bits de données, pas de parité et 1 bit de stop */
   termios_new.c_cflag &= ~(CSIZE | CSTOPB | PARENB);
   /* Valide le contrôle de flux matériel et ignore DCD */
   termios_new.c_cflag |= (CS8 | CREAD | CLOCAL | CRTSCTS);
   /* Liaison à 115200 ou 57600 bps */
   if (speed == 115200)
   {
      cfsetspeed(&termios_new, B115200);
   }
   else
   {
      cfsetspeed(&termios_new, B57600);
   }
   /* Inhibe le contrôle de flux XON/XOFF */
   termios_new.c_iflag &= ~(IXON | IXOFF | IXANY);
   /* Mode non-canonique sans echo */
   termios_new.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
   /* Pas de mode de sortie particulier */
   termios_new.c_oflag = 0;
   /* Caractères immediatement disponibles */
   termios_new.c_cc[VMIN] = 1;
   termios_new.c_cc[VTIME] = 0;

   if (tcflush(fd, TCIOFLUSH) == -1)
   {
      tmp = write(STDERR_FILENO, "Failed to flush device ", 23);
      tmp = write(STDERR_FILENO, Device, strlen(Device));
      tmp = write(STDERR_FILENO, "\r\n", CR_LF_LEN);
      close(fd);
      return(-1);
   }

   /* Mise en place des nouveaux paramètres */
   if (tcsetattr(fd, TCSANOW, &termios_new) == -1)
   {
      tmp = write(STDERR_FILENO, "Failed to set new attributes", 28);
      tmp = write(STDERR_FILENO, "\r\n", CR_LF_LEN);
      close(fd);
      return(-1);
   }

   /* Ligne RTS active */
   ioctl(fd, TIOCMBIS, &RTS_flag);

   /* Ouverture du clavier */
   openKeyboard();

   /* Affichage sur le terminal */
   tmp = write(STDOUT_FILENO,"HBTerm V", 8);
   tmp = write(STDOUT_FILENO, VERSION, VERSION_LEN);
   tmp = write(STDOUT_FILENO,": Use Ctrl-U to upload S-Record file, Ctrl-C to quit\r\n", 54);

   if (!GetPrompt(fd))
   {
      /* Lecture du caractère espace sur la liaison série */
      tmp = read(fd, &c, 1);

      if ((n = strlen(SRecordName)) > 0)
      {
         tmp = write(STDOUT_FILENO, "\r\nUploading ", 12);
         tmp = write(STDOUT_FILENO, SRecordName, n);
         tmp = write(STDOUT_FILENO, "...", 3);

         setvbuf(stdout, NULL, _IONBF, 0);

         /* Téléverse le fichier S-RECORD */
         if ((n = UploadSRecord(SRecordName, fd)) == UPLOAD_OK)
         {
            tmp = write(STDOUT_FILENO, "Upload complete!\r\n", 18);

            // Affiche l'invite de commande de Tutor
            tmp = write(STDOUT_FILENO, EchoBuffer, strlen(EchoBuffer));
         }
         else
         {
            tmp = write(STDERR_FILENO, "\n\rUpload failed!\r\n", 18);

            /* Transmission de HE pour recevoir le prompt */
            if ((n == FILE_NOT_FOUND) ||(n == INVALID_MSR) || (n == DATA_ERROR))
            {
               if (GetPrompt(fd))
               {
                  exit = -1;
               }
            }
            else
            {
               /* La carte ne répond pas, quitte l'application */
               exit = -1;
            }
         }
      }
      else if (tmp != -1)
      {
         /* Display SPACE character after > */
         tmp = write(STDOUT_FILENO, &c, 1);
      }

      device_timeout.tv_sec = 0;

      /* Boucle de lecture */
      while (!exit)
      {
         /* Définition du délai d'attente d'un caractère soit 85 µs pour 115200 b/s */
         if (speed == 57600)
         {
            device_timeout.tv_usec = 170;
         }
         else
         {
            device_timeout.tv_usec = 85;
         }

         /* Initialise le descripteur */
         FD_ZERO(&device_read_handles);

         /* Affecte le descripteur à la liaison série */
         FD_SET(fd, &device_read_handles);

         tmp = select(fd + 1, &device_read_handles, NULL, NULL, &device_timeout);

         if (tmp == -1)
         {
            tmp = write(STDERR_FILENO, "\r\nDevice error\r\n", 16);
            exit = -1;
            break;
         }
         else if ((tmp >= 1) && (FD_ISSET(fd, &device_read_handles)  ?  1  :  0))
         {
            /* Lecture du caractère sur la liaison série */
            tmp = read(fd, &c, 1);

            tmp = write(STDOUT_FILENO, &c, 1);

            setvbuf(stdout, NULL, _IONBF, 0); /* -> fflush(stdout); */
         }

         /* Si une touche est pressée */
         if (kbhit())
         {
            /* Lire le caractère */
            c = (char)getch();

            /* Fin si CTRL-C */
            if (c == -1)
            {
               break;
            }

            /* Transmission d'un break si CTRL-W */
            if (c == CWERASE)
            {
               /* Transmet un break */
               tcsendbreak(fd, 0);
            }
            else if (c == CKILL) /* Chargement d'un fichier S-Record si CTRL-U */
            {
               tmp = write(STDOUT_FILENO, "\r\nUpload S-Record file, enter file name (.s19, .s28 or .msr): ", STR_UPLOAD_LEN);

               setvbuf(stdout, NULL, _IONBF, 0);

               /* Compteur de caractères à 0 */
               n = 0;

               do
               {
                  /* Lecture du caractère */
                  tmp = read(STDIN_FILENO, &SRecordName[n], 1);

                  /* Gestion du backspace */
                  if ((SRecordName[n] == BKSP) && (n > 0))
                  {
                     tmp = write(STDOUT_FILENO, "\b \b", 3);
                     n--;
                  }
                  else if (SRecordName[n] == ESC)
                  {
                     /* Gestion du ESC et des flèches */
                     if (kbhit())
                     {
                        /* Traitement pour les flèches */
                        tmp = read(STDIN_FILENO, &SRecordName[n], 1);
                        tmp = read(STDIN_FILENO, &SRecordName[n], 1);
                     }
                  }
                  else
                  {
                     /* Affichage du caractère */
                     tmp = write(STDOUT_FILENO, &SRecordName[n++], 1);
                  }

                  if ((n != 0)  && (SRecordName[n - 1] == '\n'))
                  {
                     break;
                  }
               }
               while (n < NAME_MAX);

               if (n == NAME_MAX)
               {
                  tmp = write(STDERR_FILENO, "File name is too long!\r\n", 24);
               }
               else
               {
                  /* Remplace LF par 0 */
                  SRecordName[n - 1] = '\0';
                  tmp = write(STDOUT_FILENO, "Uploading ", 10);
                  tmp = write(STDOUT_FILENO, SRecordName, n);
                  tmp = write(STDOUT_FILENO, "...", 3);

                  setvbuf(stdout, NULL, _IONBF, 0);

                  /* Téléverse le fichier S-RECORD */
                  if ((n = UploadSRecord(SRecordName, fd)) == UPLOAD_OK)
                  {
                     tmp = write(STDOUT_FILENO, "Upload complete!\r\n", 18);

                     // Affiche l'invite de commande de Tutor
                     tmp = write(STDOUT_FILENO, EchoBuffer, strlen(EchoBuffer));
                  }
                  else
                  {
                     tmp = write(STDERR_FILENO, "\r\nUpload failed!\r\n", 18);

                     /* Transmission de HE pour recevoir le prompt */
                     if ((n == FILE_NOT_FOUND) ||(n == INVALID_MSR) || (n == DATA_ERROR))
                     {
                        if (GetPrompt(fd))
                        {
                           exit = -1;
                        }
                     }
                     else
                     {
                        /* La carte ne répond pas, quitte l'application */
                        exit = -1;
                     }
                  }
               }
            }
            else
            {
               /* Si LF, le remplacer par CR */
               if (c == '\n')
               {
                  c = '\r';
               }

               /* Si BACKSPACE, le remplacer par '\b' */
               if (c == BKSP)
               {
                  c = '\b';
               }

               if (c != -1)
               {
                  /* Transmission de la donnée sur la liaison série */
                  n = write(fd, &c, 1);
                  tcdrain(fd);
               }
            }
         }
      }
   }
   else
   {
      exit = -1;
   }

   /* Restauration des paramètres d'origine */
   tcsetattr(fd, TCSANOW, &termios_org);

   if (exit == -1)
   {
      tmp = write(STDOUT_FILENO, "Closing device, please wait...", 30);

      setvbuf(stdout, NULL, _IONBF, 0); /* -> fflush(stdout); */
   }

   /* Fermeture */
   close(fd);

   if (exit == -1)
   {
      tmp = write(STDOUT_FILENO, "\r\n", CR_LF_LEN);
   }

   closeKeyboard();

   /* Bye... */
   return(exit);
}

int InvalidParameter(char *parameter)
{
   int tmp;

   tmp = write(STDERR_FILENO, "Invalid parameter ", 18);
   tmp = write(STDERR_FILENO, parameter, strlen(parameter));
   tmp = write(STDERR_FILENO, " \r\n", CR_LF_LEN);
   tmp = -1;

   return(tmp);
}

int GetPrompt(int fd)
{
   int n, tmp, readRetry;

   /* Prépare la commande he */
   n = write(fd, "he\r", 3);
   tcdrain(fd);

   n = 0;
   readRetry = 0;

   do
   {
      tmp = read(fd, &EchoBuffer[n], 1);

      if (tmp != -1)
      {
         if (EchoBuffer[n++] == '>')
         {
             break;
         }
      }
      else
      {
         /* Incrémente le compteur d'essais */
         readRetry++;
      }
   }
   while (readRetry < nbRetry * HE_DATA);

   if (readRetry >= nbRetry * HE_DATA)
   {
      tmp = write(STDERR_FILENO, "The board is mute, exit application\r\n", 37);
      n = -1;
   }
   else
   {
      /* Affiche le prompt */
      tmp = write(STDOUT_FILENO, &EchoBuffer[n - PROMPT_LEN], PROMPT_LEN);
      setvbuf(stdout, NULL, _IONBF, 0); /* -> fflush(stdout); */
      n = 0;
   }

   return(n);
}

void delayMs(int milliseconds)
{
   long pause;
   clock_t now, then;

   pause = milliseconds * (CLOCKS_PER_SEC / 1000);
   now = then = clock();
   while ((now - then) < pause )
   {
       now = clock();
   }
}

int UploadSRecord(const char *srecName, int fd)
{
   int tmp;
   int loadSent = 0;
   unsigned int length, readRetry, nbReceived, nbRemaining;
   FILE *srec;
   char msrLine[MSR_MAX_LEN];

   if ((srec = fopen(srecName, "r")) == NULL)
   {
      tmp = write(STDERR_FILENO, "\r\nUnable to open the S-Record file ", 35);
      tmp = write(STDERR_FILENO, srecName, strlen(srecName));
      tmp = FILE_NOT_FOUND;
      return(tmp);
   }

   /* Prêt à téléverser, affiche CR LF */
   tmp = write(STDOUT_FILENO, "\r\n", CR_LF_LEN);

   while (fgets(msrLine, MSR_MAX_LEN, srec) != NULL)
   {
      if (msrLine[0] != 'S')
      {
         tmp = write(STDERR_FILENO, "Invalid S-Record file format", 28);
         fclose(srec);
         tmp = INVALID_MSR;
         return(tmp);
      }

      if (!loadSent)
      {
         /* Prépare la commande lo */
         strcpy(EchoBuffer, "lo\r");

         /* Vide tout caractère restant */
         tcflush(fd, TCIFLUSH);

         /* Transmission de lo et CR */
         tmp = write(fd, EchoBuffer, LOAD_LEN - 1);
         tcdrain(fd);

         /* Réception de l'écho */
         readRetry = 0;

         nbReceived = 0;

         do
         {
            tmp = read(fd, &EchoBuffer[nbReceived], 1);

            // Ignore l'espace d'en-tête
            if ((tmp == 1) && (EchoBuffer[0] == ' '))
            {
               tmp = -1;
               EchoBuffer[0] = '\0';
            }

            if (tmp != -1)
            {
               nbReceived += tmp;

               if (nbReceived == LOAD_LEN)
               {
                  break;
               }
            }
            else
            {
               /* Incrémente le compteur d'essais */
               readRetry++;
            }
         }
         while (readRetry < nbRetry * LOAD_LEN);

         /* Force la fin de chaîne */
         EchoBuffer[LOAD_LEN] = '\0';

         if (readRetry >= nbRetry * LOAD_LEN)
         {
            tmp = write(STDERR_FILENO, "The board is mute, exit application", 35);
            fclose(srec);
            tmp = BOARD_MUTE;
            return(tmp);
         }
         else if (strcmp("LO\r\n", EchoBuffer))
         {
            tmp = write(STDERR_FILENO, "Invalid board response", 22);
            fclose(srec);
            tmp = DATA_ERROR;
            return(tmp);
         }

         /* Commande lo OK */
         loadSent = 1;
      }

      /* Ignore la ligne S0 */
      if ((msrLine[1] == '1') || (msrLine[1] == '2') || (msrLine[1] == '9') || (msrLine[1] == '8'))
      {
         /* lecture de la longueur */
         sscanf(&msrLine[2], "%2x", &length);

         if ((msrLine[1] == '1') || (msrLine[1] == '9'))
         {
            length -= 3;
         }
         else
         {
            length -= 4;
         }

         /* Calcule la taille des données */
         length /= 2;

         if (length > DATA_LEN)
         {
            tmp = write(STDERR_FILENO, "S-Record file must be updated since TUTOR requires 32 or less data by line", 71);
            fclose(srec);
            tmp = INVALID_MSR;
            return(tmp);
         }

         length = strlen(msrLine);

         if (msrLine[length - 1] == '\n')
         {
             if (length >= 2)
             {
                 if (msrLine[length - 2] != '\r')
                 {
                     /* Remplace le Line Feed par un Carriage Return */
                     msrLine[length - 1] = '\r';
                 }
                 else
                 {
                     /* Supprime le Line Feed */
                     msrLine[length - 1] = '\0';
                     length--;
                 }
             }

             if (msrLine[length - 1] != '\r')
             {
                /* Si pas de Carriage Return, il est ajouté */
                msrLine[length - 1] = '\r';
                msrLine[length] = '\0';
                length++;
             }
         }

         /* Transmission de la ligne S-Record */
         tmp = write(fd, msrLine, length);
         tcdrain(fd);

         /* Réception de l'écho, incrémente length car TUTOR ajoute un LINE FEED */
         length++;

         readRetry = 0;

         nbReceived = 0;

         nbRemaining = length;

         do
         {
            tmp = read(fd, &EchoBuffer[nbReceived], nbRemaining);

            if (tmp != -1)
            {
               nbReceived += tmp;
               nbRemaining -= tmp;

               if (nbReceived == length)
               {
                  break;
               }
            }
            else
            {
               /* Incrémente le compteur d'essais */
               readRetry++;
            }
         }
         while (readRetry < nbRetry * length);

         if (readRetry >= nbRetry * length)
         {
            /* Erreur de timeout */
            tmp = write(STDERR_FILENO, "The board is mute, exit application", 35);
            fclose(srec);
            tmp = BOARD_MUTE;
            return(tmp);
         }
         else
         {
            /* Force la fin de chaîne et supprime le LINE FEED */
            EchoBuffer[length - 1] = '\0';
         }

         if (strcmp(EchoBuffer, msrLine))  /* Vérifie les données retournées */
         {
            /* Erreur pendant le téléversement */
            tmp = write(STDERR_FILENO, "Invalid data detected during upload", 35);
            fclose(srec);
            tmp = DATA_ERROR;
            return(tmp);
         }
         else
         {
            /* Et affiche la ligne et un LINE FEED*/
            tmp = write(STDOUT_FILENO, EchoBuffer, length);
            tmp = write(STDOUT_FILENO, "\n", 1);
         }
      }
   }

   /* Réception et mémorisation de l'invite de commande */
   readRetry = 0;

   nbReceived = 0;

   do
   {
      tmp = read(fd, &EchoBuffer[nbReceived], 1);

      if (tmp != -1)
      {
         nbReceived += tmp;

         if (nbReceived == PROMPT_LEN)
         {
            break;
         }
      }
      else
      {
         /* Incrémente le compteur d'essais */
         readRetry++;
      }
   }
   while (readRetry < nbRetry * PROMPT_LEN);

   if (readRetry >= nbRetry * PROMPT_LEN)
   {
      tmp = write(STDERR_FILENO, "The board is mute, exit application", 35);
      fclose(srec);
      tmp = BOARD_MUTE;
      return(tmp);
   }

   EchoBuffer[PROMPT_LEN] = '\0';

   /* Fermeture du fichier */
   fclose(srec);
   return(0);
}
