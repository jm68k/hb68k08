# HB68K08 SINGLE BOARD 8-BIT COMPUTER

Homebrew retro computer HB68K08

![Image of HB68K08](HB68K08.png)

The HB68K08 microcomputer is an open hardware design for educational purposes for makers.
It has no surface mounted devices and only the memory that contains the code to be executed needs to be programmed.
Based on the MC68008 (or TS68008), an 8-bit microprocessor with a 32-bit internal architecture, it is optimized for running programs written in C or C++.
Expansion slots allow you to add a daughter board to access the world of connected objects.

Le micro ordinateur HB68K08 est une conception open hardware à but pédagogique pour les makers.
Il ne comporte aucun composants montés en surface et seule la mémoire qui contient le code à exécuter doit être programmée.
Basé sur le MC68008 (ou TS68008), un microprocesseur 8-bit avec une architecture interne 32-bit, il est optimisé pour l'exécution de programmes écrits en C ou C++.
Des connecteurs d'extension permettent d'ajouter une carte fille pour accéder au monde des objets connectés.
